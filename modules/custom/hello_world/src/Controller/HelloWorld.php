<?php

// PSR-4 Namespacing Standard
namespace Drupal\hello_world\Controller;

// Importing a class dependency for inheritance
use Drupal\Core\Controller\ControllerBase;

/**
 * Class HelloWorld.
 *
 * @package Drupal\hello_world\Controller
 */
class HelloWorld extends ControllerBase {
  /**
   * Content.
   *
   * @return string
   *   Return Hello string.
   */
  public function content() {
    return [
      '#type' => 'markup',
      '#markup' => 'Hello World!',
    ];

  }

}
