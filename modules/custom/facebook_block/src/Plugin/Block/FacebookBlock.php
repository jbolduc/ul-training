<?php

namespace Drupal\facebook_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'FacebookBlock' block.
 *
 * @Block(
 *  id = "facebook_block",
 *  admin_label = @Translation("Facebook block"),
 * )
 */
class FacebookBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Get module path.
    $module_path = drupal_get_path('module', 'facebook_block');

    // Get configuration:
    $config = $this->getConfiguration();

    // Render Facebook link.
    $build = [];
    $build['facebook_block']['#markup'] = '<a href = "' . $config['url'].'" target = "_blank"><img src = "/' . $module_path . '/images/FB_FindUsOnFacebook-' . $config['size'] .'.png"></a>';

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $config = $this->getConfiguration();

    $form['url'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('URL'),
      '#description' => $this->t('Provide the full URL to your facebook page.'),
      '#required' => TRUE,
      '#default_value' => $config['url'],
    );
    $form['size'] = array(
      '#type' => 'select',
	  '#title' => $this->t('Size'),
      '#description' => $this->t('What size is your image?'),
      '#options' => array(
        '100' => '100 x 26',
        '114' => '114 x 29',
        '144' => '144 x 37',
        '320' => '320 x 82',
        '512' => '512 x 132',
        '1024' => '1024 x 264',
      ),
      '#default_value' => $config['size'],
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['url'] = $form_state->getValue('url');
    $this->configuration['size'] = $form_state->getValue('size');
  }
}
